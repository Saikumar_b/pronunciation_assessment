# import declaration
from fastapi import FastAPI, Form, Request
from fastapi.responses import PlainTextResponse, HTMLResponse, FileResponse
#from fastapi.templating import Jinja2Templates
from fastapi import FastAPI, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from timing_asgi import TimingMiddleware, TimingClient
from timing_asgi.integrations import StarletteScopeToName
from fastapi.responses import JSONResponse
import boto3
from botocore.exceptions import ClientError
from  subprocess import Popen,PIPE
import shutil
from pathlib import Path
from tempfile import NamedTemporaryFile
import os
import re
from collections import defaultdict
import math


#initialization
app = FastAPI()

# Jinja2 template instance for returning webpage via template engine
#templates = Jinja2Templates(directory="templates")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
# hello world, GET method, return string
@app.get("/", response_class=PlainTextResponse)
async def hello():
    return "Hello World!"

class PrintTimings(TimingClient):
    def timing(self, metric_name, timing, tags):
        print("time taken for :", tags[1],"request is:- ",timing )

@app.post("/gop/")
async def get_gop(upload_file:UploadFile = File(...),email: str = Form(...),text:str=Form(...)):
    if upload_file.filename.endswith('.wav') or upload_file.filename.endswith('.mp3'):
        #saving temporary file
        try:
            suffix = Path(upload_file.filename).suffix
            with NamedTemporaryFile(delete=False, suffix=suffix) as tmp:
                shutil.copyfileobj(upload_file.file, tmp)
                tmp_path = Path(tmp.name)
        finally:
            upload_file.file.close()

        #saving file to aws 
        try:
            # YOUR AWS IAM ACCESS KEY AND SECRET, GET IT FROM THE IAM CONSOLE
            s3_client = boto3.client('s3', aws_access_key_id='*****************', aws_secret_access_key='*************************')
            filename=email.split('@')[0].replace('.', '_') + '_sep_' + upload_file.filename
            file_name='/home/ec2-user/test/'+filename[:-3]+'wav'

            # storing into BUCKET
            response=s3_client.upload_file(str(tmp_path),'bucket_name','incoming/test/'+filename)
            #storing text file

            text_dta ="{}'{}'".format("b",str(text.upper()))
            response1=s3_client.put_object(Body=text_dta, Bucket='bucket_name', Key='incoming/test/'+filename[:-3]+'txt')

        except ClientError as e:
                print("S3 file uploading error",e)
        try:
            process = Popen(('/home/ec2-user/ffmpeg','-y', '-i', tmp_path,'-ar','16000',file_name))
            stdout, stderr = process.communicate()
            if stderr:
                print(stderr)
            with open('/home/ec2-user/models/data/test/text', 'w+') as f:
                text=re.sub('\W+',' ', text)
                l1 = "{}  {}".format("user_t1", str(text.upper()))
                f.write(l1)
            with open('/home/ec2-user/models/data/test/wav.scp', 'w+') as f:
                l1 = "{}  {}".format("user_t1",file_name)
                f.write(l1)

            os.system('cp -r /home/ec2-user/models/data/test/ /home/ec2-user/models/data/eval2000')
            process2 = Popen(('/home/ec2-user/kaldi/kaldi/egs/gop/s5/gop_run.sh',file_name), stdout=PIPE,stderr=PIPE, shell=True)
            stdout, stderr = process2.communicate()
            if stderr:
                print(stderr)
            else:
                print(stdout)
                # raise Exception("Error " + str(stderr))

            # computing GoP score from GoP output files
            # final dictionary
            final_dict = {}
            # creating a dictionary with pure-phone id and it's corressponding phone (sil-> 3)
            with open('/home/ec2-user/reqd_files/phones-pure.txt') as pp:
                pure_phones = pp.read()
            pure_phone_dict = {}
            pp_rows = pure_phones.split('\n')
            for pp in pp_rows:
                if pp:
                    pure_phone_dict[int(pp.split('\t')[1])] = str(pp.split('\t')[0])
                # getting GoP data
            with open('/home/ec2-user/output/gop.1.txt') as g:
                gop_data = g.read()

            lines = gop_data.split("\n")
            dp = re.findall(r'\[(.*?)\]', lines[0])
            # storing phones and it's values to separate lists
            total_score = 0
            num_phones=0
            gop_scores=defaultdict(list)
            num_silence=0
            for i in dp:
                z = i.split()
                if float(z[0]) > 2:  # removing silence phones
                    gop_scores[pure_phone_dict.get(int(z[0]))].append(math.exp(float(z[1])/0.5)*100)
                    total_score+=math.exp(float(z[1])/0.5)*100
                    num_phones=num_phones+1
                else:
                    num_silence=num_silence+1
            print("score is:- ",total_score//num_phones)
            final_dict['overall_GoP_score']=total_score//num_phones
            final_dict['no_of_silences']=num_silence
            final_dict['number_of_phones']=num_phones
            final_dict['gop_phoneme_scores']=dict(gop_scores)
            
            print(final_dict)
            #storing in output directory on s3
            response=s3_client.upload_file('/home/ec2-user/output/gop.1.txt','bucket_name','outgoing/test/'+filename[:-3]+'gop.txt')
            # removing files for next run
            os.system("rm -r /home/ec2-user/output/gop.1.txt  {file_}".format(file_=file_name))
            return JSONResponse(content=final_dict)

        except Exception as er:
            if str(er)=="integer division or modulo by zero":
                return "Unable to decode the audio"
            print(er)
            return "error occured while  calculating GoP"

    else:
        return "Please Upload either .mp3 or .wav file "

app.add_middleware(
    TimingMiddleware,
    client=PrintTimings(),
    metric_namer=StarletteScopeToName(prefix="myapp", starlette_app=app)
)

# main
if __name__ == '__main__':
    uvicorn.run('main:app', host='0.0.0.0', port=8080)