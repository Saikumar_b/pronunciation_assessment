# gop
Compute Goodness of Pronunciation using Kaldi

### Installation

1.  Move these files to kaldi/egs/gop
2.  change the file paths in gop_run.sh and and main.py
3.  install pip
4.	install requirements for fastapi using
	```pip install -r requirements.txt```
5.	run the app using gunicorn 
	```gunicorn -w 2 --reload --bind 0.0.0.0:8080 --capture-output  --error-logfile error_log.txt  --access-logfile log.txt -k uvicorn.workers.UvicornWorker main:app```
6.	open localhost:8080/docs to check the docs







